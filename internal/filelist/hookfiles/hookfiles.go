package hookfiles

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/postmarketOS/postmarketos-mkinitfs/internal/filelist"
	"gitlab.com/postmarketOS/postmarketos-mkinitfs/internal/misc"
)

type HookFiles struct {
	filePath string
}

// New returns a new HookFiles that will use the given path to provide a list
// of files + any binary dependencies they might have.
func New(filePath string) *HookFiles {
	return &HookFiles{
		filePath: filePath,
	}
}

func (h *HookFiles) List() (*filelist.FileList, error) {
	log.Printf("- Searching for file lists from %s", h.filePath)

	files := filelist.NewFileList()
	fileInfo, err := os.ReadDir(h.filePath)
	if err != nil {
		log.Println("-- Unable to find dir, skipping...")
		return files, nil
	}
	for _, file := range fileInfo {
		path := filepath.Join(h.filePath, file.Name())
		f, err := os.Open(path)
		if err != nil {
			return nil, fmt.Errorf("getHookFiles: unable to open hook file: %w", err)

		}
		defer f.Close()
		log.Printf("-- Including files from: %s\n", path)

		if list, err := slurpFiles(f); err != nil {
			return nil, fmt.Errorf("hookfiles: unable to process hook file %q: %w", path, err)
		} else {
			files.Import(list)
		}
	}
	return files, nil
}

func slurpFiles(fd io.Reader) (*filelist.FileList, error) {
	files := filelist.NewFileList()

	s := bufio.NewScanner(fd)
	for s.Scan() {
		line := s.Text()
		if len(line) == 0 || strings.HasPrefix(line, "#") {
			continue
		}

		src, dest, has_dest := strings.Cut(line, ":")

		fFiles, err := misc.GetFiles([]string{src}, true)
		if err != nil {
			return nil, fmt.Errorf("unable to add %q: %w", src, err)
		}
		// loop over all returned files from GetFile
		for _, file := range fFiles {
			if !has_dest {
				files.Add(file, file)
			} else if len(fFiles) > 1 {
				// Don't support specifying dest if src was a glob
				// NOTE: this could support this later...
				files.Add(file, file)
			} else {
				// dest path specified, and only 1 file
				files.Add(file, dest)
			}
		}
	}

	return files, s.Err()
}
